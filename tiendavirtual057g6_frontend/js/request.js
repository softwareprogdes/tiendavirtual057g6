//const url = "https://minticloud.uis.edu.co/c3s57grupo6/"   //ejecutar servidor
const url = "http://localhost:8080/"    //ejecutar localmente


function  sendRequest(endPoint, method, data){
    let request = new XMLHttpRequest();
    request.open(method, url+endPoint);
    request.responseType = 'json';
    request.setRequestHeader('Content-Type','application/json');
    request.send(data ? JSON.stringify(data): data);
    return request
}