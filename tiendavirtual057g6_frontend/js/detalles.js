
function loadData(){
    let request = sendRequest('detalle/list', 'GET', '')
    let table = document.getElementById('detalles-table');
    table.innerHTML = "";
    request.onload = function(){
        
        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr>
                    <th>${element.idDetalle}</th>
                    <td>${element.producto.idProducto}</td>
                    <td>${element.transaccion.idTransaccion}</td>
                    <td>${element.valorDetalle}</td>
                    <td>${element.cantidadDetalle}</td>
                    <td>${element.totalDetalle}</td>
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "./form_detalles.html?id=${element.idDetalle}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deleteTransaccion(${element.idDetalle})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos.</td>
            </tr>
        `;
    }
}

function loadDetalles(idDetalle){
    let request = sendRequest('detalle/list/'+idDetalle, 'GET', '')
    let producto = document.getElementById('detalles-producto')
    let transaccion = document.getElementById('detalles-transaccion')
    let valor = document.getElementById('detalles-valor')
    let cantidad = document.getElementById('detalles-cantidad')
    let total = document.getElementById('detalles-total')
    let id = document.getElementById('detalles-id')
    request.onload = function(){
        
        let data = request.response
        id.value = data.idDetalle
        producto.value = data.idProducto
        transaccion.value = data.idTransaccion
        valor.value = data.valorDetalle
        cantidad.value = data.cantidadDetalle
        total.value=data.totalDetalle
    }
    request.onerror = function(){
        alert("Error al recuperar los datos.");
    }
}

function deleteDetalles(idDetalle){
    let request = sendRequest('detalle/'+idDetalle, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}

function saveDetalles(){
    let producto = document.getElementById('detalles-producto').value
    let transaccion = document.getElementById('detalles-transaccion').value
    let valor = document.getElementById('detalles-valor').value
    let cantidad = document.getElementById('detalles-cantidad').value
    let total = document.getElementById('detalles-total').value
    let id = document.getElementById('detalles-id').value
    let data = {'idDetalle': id,'idProducto':producto,'idTransaccion': transaccion, 'valorDetalle': valor, 'cantidadDetalle':cantidad, 'totalDetalle':total}
    let request = sendRequest('detalle/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'detalles.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios.')
    }
}