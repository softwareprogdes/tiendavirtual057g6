
function loadData(){
    let request = sendRequest('transaccion/list', 'GET', '')
    let table = document.getElementById('transaccion-table');
    table.innerHTML = "";
    request.onload = function(){
        
        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr>
                    <th>${element.idTransaccion}</th>
                    <td>${element.tipoTransaccion}</td>
                    <td>${element.fechaTransaccion}</td>
                    <td>${element.vendedor}</td>
                    <td>${element.comprador}</td>
                    <td>${element.total}</td>
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "form_transaccion.html?id=${element.idTransaccion}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deleteTransaccion(${element.idTransaccion})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos.</td>
            </tr>
        `;
    }
}

function loadTransaccion(idTransaccion){
    let request = sendRequest('transaccion/list/'+idTransaccion, 'GET', '')
    let tipo = document.getElementById('transaccion-tipo')
    let fecha = document.getElementById('transaccion-fecha')
    let vendedor = document.getElementById('transaccion-vendedor')
    let comprador = document.getElementById('transaccion-comprador')
    let total = document.getElementById('transaccion-total')
    let id = document.getElementById('transaccion-id')
    request.onload = function(){
        
        let data = request.response
        id.value = data.idTransaccion
        tipo.value = data.tipoTransaccion
        fecha.value = data.fechaTransaccion
        vendedor.value = data.vendedor
        comprador.value = data.comprador
        total.value=data.total
    }
    request.onerror = function(){
        alert("Error al recuperar los datos.");
    }
}

function deleteTransaccion(idTransaccion){
    let request = sendRequest('transaccion/'+idTransaccion, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}

function saveTransaccion(){
    let tipo = document.getElementById('transaccion-tipo').value
    let fecha = document.getElementById('transaccion-fecha').value
    let vendedor = document.getElementById('transaccion-vendedor').value
    let comprador = document.getElementById('transaccion-comprador').value
    let total = document.getElementById('transaccion-total').value
    let id = document.getElementById('transaccion-id').value
    let data = {'idTransaccion': id,'tipoTransaccion':tipo,'fechaTransaccion': fecha, 'vendedor': vendedor, 'comprador':comprador, 'total':total}
    let request = sendRequest('transaccion/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'transaccion.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios.')
    }
}